<?php

/*
 * This file is part of Qdevtools
 * Copyright (C) 2019-2020  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\modeling\mapper;

use Ada\mapper\AdaDataMapper,
    Ada\adapters\XmlAdapter,
    Qerapp\qbasic\model\modeling\entity\ModelEntity,
    Qerapp\qbasic\model\module\mapper\ModuleXmlMapper AS ModuleMapper;

/**
 * *****************************************************************************
 * Description of ModelMapper
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class ModelXmlMapper extends AdaDataMapper {

    protected
            $_ModuleMapper;

    public function __construct(bool $load_module = true) {
        $data_xml = __DATA__ . '/xml/qbasic/models';
        parent::__construct(new XmlAdapter($data_xml, 'model', false));

        ($load_module) ? $this->_ModuleMapper = new ModuleMapper : $this->_ModuleMapper = null;
    }

    /**
     * Find one model
     * @param string $model
     * @return type
     */
    public function findModel(string $model) {
        return $this->findOne(['model_name' => $model]);
    }

    /**
     * save a model
     * @param ModelEntity $Model
     */
    public function save(ModelEntity $Model) {

        $data = parent::getDataObject($Model);

        if (is_null($Model->id_model)) {
            $Model->id_model = $this->_Adapter->insert($data);
        } else {
            $this->_Adapter->update($data, ['id_model' => $Model->id_model]);
        }
    }

    /**
     * Delte a model
     * @param type $model
     */
    public function delete($model) {

        if ($model instanceof ModelEntity) {
            $id = $model->id_model;
        } else {
            $id = $model;
        }
        return $this->_Adapter->delete(['id_model' => $id]);
    }

    public function createEntity(array $row): ModelEntity {

        $Module = (!is_null($this->_ModuleMapper)) ? $this->_ModuleMapper->findByName($row['module']) : null;

        $ModelEntity = new ModelEntity($row, $Module);
        return $ModelEntity;
    }

}
