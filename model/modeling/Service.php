<?php

/*
 * This file is part of Qdevtools
 * Copyright (C) 2019-2020  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\modeling;

use Qerapp\qbasic\model\modeling\entity\Entity;

/**
 * *****************************************************************************
 * Description of Service
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class Service
{

    const
            _SERVICE_LAYOUT_ = __QERAPPSFOLDER__. 'qbasic/_layouts/tpl_service.qer';

    protected
            $_Entity,
            $_service_name,
            $_service_path,
            $_service_namespace;

    public function __construct(Entity $Entity)
    {
        $this->_Entity = $Entity;
        $this->_service_name = $this->_Entity->entity_name . 'Service';
        $this->_service_path = realpath($this->_Entity->Model->model_path);
        $this->_service_namespace = str_replace('','',$this->_Entity->Model->model_namespace);
      
    }

    
    /**
     * -------------------------------------------------------------------------
     * Create service class
     * -------------------------------------------------------------------------
     */
    public function createService()
    {

        // first create the file
        $full_path_service = $this->_service_path . '/' . $this->_service_name . '.php';
        \helpers\File::createFile($full_path_service);

        // fill the content of service
        $replaces = [
            '[{service_namespace}]' => $this->_service_namespace,
            '[{entity_namespace}]' => $this->_Entity->entity_namespace . '\\' . $this->_Entity->entity_name . 'Entity',
            '[{repository_namespace}]' => $this->_Entity->Model->model_namespace. '\\repository\\' . $this->_Entity->entity_name . 'Repository',
            '[{mapper_namespace}]' => $this->_Entity->Model->model_namespace. '\\interfaces\\' . $this->_Entity->entity_name . 'Mapper',
            '[{mapper_namespace_inject}]' => $this->_Entity->Model->model_namespace. '\\mapper\\' . $this->_Entity->entity_name.'Mapper',
            '[{service_name}]' => $this->_service_name,
            '[{mapper_name}]' =>  $this->_Entity->entity_name.'Mapper',
            '[{entity_name}]' => $this->_Entity->entity_name,
            '[{repository_name}]' => $this->_Entity->entity_name.'Repository',
            '[{date}]' => date('Y-m-d H:i:s'),
        ];
        $content_class = strtr(file_get_contents(realpath(self::_SERVICE_LAYOUT_)), $replaces);
        file_put_contents(realpath($full_path_service), $content_class);
    }

}
