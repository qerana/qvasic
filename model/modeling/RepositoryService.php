<?php

/*
 * This file is part of Qdevtools
 * Copyright (C) 2019-2020  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\modeling;

use Qerapp\qbasic\model\modeling\entity\Entity;

/**
 * *****************************************************************************
 * Description of RepositoryClass
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class RepositoryService
{

    const
            _REPOSITORY_LAYOUT_ = __QERAPPSFOLDER__. 'qbasic/_layouts/tpl_repository.qer',
            _REPOSITORY_INTERFACE_ = __QERAPPSFOLDER__. 'qbasic/_layouts/tpl_repository_interface.qer';

    protected
            $_Entity,
            $_repository_path,
            $_repository_name,
            $_repository_namespace,
            $_finders,
            $_i_finders;

    public function __construct(Entity $Entity)
    {
        $this->_Entity = $Entity;
        $this->_repository_path = realpath($this->_Entity->Model->model_path . '/repository');
        $this->_repository_name = $this->_Entity->entity_name . 'Repository';
        $this->_repository_namespace = $this->_Entity->Model->model_namespace . '\\repository';
    }

    /**
     * -------------------------------------------------------------------------
     * Create repository
     * -------------------------------------------------------------------------
     */
    public function createRespository()
    {

        $path_repo = $this->_repository_path . '/' . $this->_repository_name . '.php';
        $path_repo_interface = $this->_Entity->Model->model_path . '/interfaces/' . $this->_repository_name . 'Interface.php';

        // create mapper and mapper interface
        \helpers\File::createFile($path_repo);
        \helpers\File::createFile($path_repo_interface);
        
        $this->builFinders();

        $replaces = [
            '[{repository_namespace}]' => $this->_repository_namespace,
            '[{repository_namespace_interface}]' => $this->_Entity->Model->model_namespace.'\\interfaces',
            '[{mapper_namespace}]' => $this->_Entity->entity_namespace . 'mapper\\'.$this->_Entity->entity_name.'Mapper',
            '[{mapper}]' => $this->_Entity->entity_name.'MapperInterface',
            '[{mapper_interface_namespace}]' => $this->_Entity->Model->model_namespace . '\\interfaces\\'.$this->_Entity->entity_name.'MapperInterface',
            '[{entity_namespace}]' => $this->_Entity->entity_namespace . '\\' . $this->_Entity->entity_name . 'Entity',
            '[{entity_interface_namespace}]' => $this->_Entity->Model->model_namespace.'\\interfaces'. '\\' . $this->_Entity->entity_name . 'Interface',
            '[{repository_name}]' => $this->_repository_name,
            '[{Entity}]' => $this->_Entity->entity_name,
            '[{Entity_key}]' => $this->_Entity->entity_key,
            '[{table_name}]' => $this->_Entity->source,
            '[{date}]' => date('Y-m-d H:i:s'),
            '[{finders}]' => $this->_finders,
            '[{i_finders}]' => $this->_i_finders
        ];

        // lets  fill the mapper
        $content_class = strtr(file_get_contents(realpath(self::_REPOSITORY_LAYOUT_)), $replaces);
        file_put_contents(realpath($path_repo), $content_class);

        // interface mapper
        $content_interface = strtr(file_get_contents(realpath(self::_REPOSITORY_INTERFACE_)), $replaces);
        file_put_contents(realpath($path_repo_interface), $content_interface);
    }

    /**
     * -------------------------------------------------------------------------
     * build the repository finders
     * -------------------------------------------------------------------------
     */
    public function builFinders()
    {


        foreach ($this->_Entity->entity_attributes AS $Attribute):

            $this->parseFinder($Attribute);

        endforeach;
    }

     /**
     * -------------------------------------------------------------------------
     * Parse a Attribute to respective finder
     * -------------------------------------------------------------------------
     * @param object $Attribute
     */
    public function parseFinder(object $Attribute)
    {

        $typehint = ($Attribute->type == 'int') ? 'int ' : 'string';
        $name = strtolower($Attribute->name);
        $this->_finders .= "/** \n"
                . "* ------------------------------------------------------------------------- \n"
                . "* Fin by  " . $Attribute->name . "\n"
                . "* ------------------------------------------------------------------------- \n"
                . "* @param " . $Attribute->name . " \n"
                . "*/ \n "
                . ' public function findBy' . ucwords($Attribute->name)
                . '(' . $typehint . ' $' . $name . ',array $options = [])' . "\n" . "{ \n"
                . 'return $this->_' . $this->_Entity->entity_name . "Mapper->findAll(['" . $name . "'" . '=> $' . $name . '],$options);' . "\n" . '}';

        // for interface only
        $this->_i_finders .= ' public function findBy' . ucwords($Attribute->name)
                . '(' . $typehint . ' $' . $name . ',array $options = []);' . "\n";
    }

}
