<?php

/*
 * This file is part of Qdevtools
 * Copyright (C) 2019-2020  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\modeling\entity;

use Ada\EntityManager,
    Qerapp\qbasic\model\modeling\entity\ModelEntity;

class Entity extends EntityManager
{

    public $Model,
            $entity_path,
            $entity_namespace,
            $entity_key,
            $entity_attributes = [];
    protected
            $_id_entity,
            $_entity_name,
            $_model,
            /** @param string source, the name of the table or file */
            $_source,
            /** @param string origin, origin of source, mysql,xml,etc */
            $_origin,
            $_relation;

    public function __construct(array $data = [], ModelEntity $Model = null, array $attributes = [])
    {

        (!empty($data)) ? $this->populate($data) : '';
        (!is_null($Model)) ? $this->_setModel($Model) : '';
        (!is_null($attributes)) ? $this->_setAttributes($attributes) : '';
    }

    //SETTERS

    /**
     * -------------------------------------------------------------------------
     * Set model object
     * -------------------------------------------------------------------------
     * @param type $Model
     */
    private function _setModel($Model)
    {
        $this->Model = $Model;
        $this->entity_path = realpath($this->Model->model_path . '/entity/');
        $this->entity_namespace = $this->Model->model_namespace . '\\entity';
    }

    /**
     * -------------------------------------------------------------------------
     * Set entity attributes
     * -------------------------------------------------------------------------
     * @param type $Attributes
     */
    private function _setAttributes($Attributes)
    {

        foreach ($Attributes AS $Attribute):
            $this->entity_attributes[] = $Attribute;
        endforeach;

        // set the entity key
        foreach ($this->entity_attributes AS $Attribute):
            if ($Attribute->obs === 'auto_increment') {
                $this->entity_key = $Attribute->name;
            }
        endforeach;
    }

    public function set_id_entity($_id_entity)
    {
        $this->_id_entity = $_id_entity;
    }

    public function set_source($_src)
    {
        $this->_source = $_src;
    }

    public function set_origin($_origin)
    {
        $this->_origin = $_origin;
    }

    public function set_relation($_relation)
    {
        $this->_relation = $_relation;
    }

    /**
     * -------------------------------------------------------------------------
     * Set entity name, only letters
     * -------------------------------------------------------------------------
     * @param type $_entity_name
     * @throws \InvalidArgumentException
     */
    public function set_entity_name($_entity_name)
    {
        // only letters in model name 
        if (ctype_alpha($_entity_name) == 1) {
            $this->_entity_name = ucfirst($_entity_name);
        } else {
            throw new \InvalidArgumentException('Only letters in entity name, resolve this, and try again :-)');
        }
    }

    public function set_model($_model)
    {
        $this->_model = $_model;
    }

    // GETTERS

    public function get_id_entity()
    {
        return $this->_id_entity;
    }

    public function get_entity_name()
    {
        return ucfirst($this->_entity_name);
    }

    public function get_model()
    {
        return $this->_model;
    }

    public function get_source()
    {
        return $this->_source;
    }

    public function get_origin()
    {
        return $this->_origin;
    }

    public function get_relation()
    {
        return $this->_relation;
    }

}
