<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\module\entity;

/*
  |*****************************************************************************
  | INTERFACE  for module
  |*****************************************************************************
  |
  | Entity module
  | @author TUPA,
  | @date 2019-11-09 06:43:37,
  |*****************************************************************************
 */

interface ModuleInterface
{

//SETTERS


    public function set_name(string $module): void;

    public function set_layout(string $layout): void;

    public function set_visible(string $visible): void;

    public function set_style(string $style): void;
    
    public function set_nav_name(string $style): void;

//GETTERS


    public function get_module();

    public function get_layout();

    public function get_visible();

    public function get_style();
    
    public function get_nav_name();
}
