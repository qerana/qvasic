<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\module\mapper;
use Qerapp\qbasic\model\module\entity\ModuleInterface;
/*
  |*****************************************************************************
  | ModuleMapperInterface
  |*****************************************************************************
  |
  | MAPPER INTERFACE Module
  | @author TUPA,
  | @date 2019-11-09 06:43:37,
  |*****************************************************************************
 */

interface ModuleMapperInterface 
{
    
    public function findById(int $id);
    
    public function findAll(array $conditions = [],array $options = []);
    
    public function findOne(array $conditions);
    
    public function save(ModuleInterface $User);
    
    public function delete($id);
 
 
}