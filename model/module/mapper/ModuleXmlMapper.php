<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\module\mapper;

use Ada\mapper\AdaDataMapper,
    Ada\adapters\XmlAdapter,
    Qerapp\qbasic\model\module\entity\ModuleEntity,
    Qerapp\qbasic\model\module\entity\ModuleInterface,
    Qerapp\qbasic\model\module\mapper\ModuleMapperInterface;

/**
 * *****************************************************************************
 * Description of ModuleMapper
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class ModuleXmlMapper extends AdaDataMapper implements ModuleMapperInterface {

    public
            $xml_structure,
            $path_xml;

    public function __construct() {
        $data_xml = __DATA__ . 'xml/qbasic/modules';
        parent::__construct(new XmlAdapter($data_xml, 'module', false));
    }

    /**
     * find module by module name
     * @param string $module
     * @return type
     */
    public function findByName(string $module) {
        return $this->findOne(['name' => $module]);
    }

    /**
     * find by id module
     * @param int $id
     * @return type
     */
    public function findById(int $id) {
        return $this->findOne(['id_module' => $id]);
    }


    /**
     * -------------------------------------------------------------------------
     * Save entity
     * -------------------------------------------------------------------------
     */
    public function save(ModuleInterface $Module) {

        $data = parent::getDataObject($Module);
        $this->_Adapter->insert($data);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete entity
     * -------------------------------------------------------------------------
     * @param mixed $module_name
     * @return type
     */
    public function delete($module_name) {

        $this->_Adapter->delete(['name' => $module_name]);
    }

    /**
     * -------------------------------------------------------------------------
     * Create Module Entity
     * -------------------------------------------------------------------------
     * @param array $Module
     * @return Entity
     */
    protected function createEntity(array $Module): ModuleEntity {

        $ModuleEntity = new ModuleEntity($Module);

        return $ModuleEntity;
    }

}
