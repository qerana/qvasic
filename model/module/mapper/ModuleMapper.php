<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\module\mapper;

use Ada\adapters\PDOAdapter,
    Ada\mapper\AdaDataMapper,
    Qerapp\qbasic\model\module\entity\ModuleEntity,
    Qerapp\qbasic\model\module\entity\ModuleInterface,
    Qerapp\qbasic\model\module\mapper\ModuleMapperInterface;

/**
 * *****************************************************************************
 * Description of ModuleMapper
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class ModuleMapper extends AdaDataMapper implements ModuleMapperInterface
{


    public function __construct()
    {

        parent::__construct(new PDOAdapter(\Ada\SqlLitePDO::singleton(), 'qer_module'));
    }

    /**
     * find module by module name
     * @param string $module
     * @return type
     */
    public function getByName(string $module)
    {

        return $this->findOne(['module' => $module]);
    }

    
    /**
     * find by id module
     * @param int $id
     * @return type
     */
    
    public function findById(int $id)
    {
        return $this->findOne(['id_module' => $id]);
    }
    
    
    public function findOne(array $conditions) {
        ;
    }
    
    public function findAll(array $conditions = array(), array $options = array()) {
        ;
    }
    
    
    /**
     * -------------------------------------------------------------------------
     * Save entity
     * -------------------------------------------------------------------------
     */
    public function save(ModuleInterface $Module)
    {

        $data = parent::getDataObject($Module);

        if (is_null($Module->id_module)) {
            $Module->id_module = $this->_Adapter->insert($data);
        } else {
            $this->_Adapter->update($data, ['id_module' => $Module->id_module]);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Delete entity
     * -------------------------------------------------------------------------
     * @param mixed $id
     * @return type
     */
    public function delete($id)
    {

        // if $id is a object and a UserEntity  
        if ($id instanceof ModuleEntity) {
            $id = $id->id_module;
        }

        return $this->_Adapter->delete(['id_module' => $id]);
    }

    /**
     * -------------------------------------------------------------------------
     * Create Entity
     * -------------------------------------------------------------------------
     * @param array $row
     * @return Entity
     */
    protected function createEntity(array $row): ModuleEntity
    {
        $ModuleEntity = new ModuleEntity($row);
        return $ModuleEntity;
    }

}
