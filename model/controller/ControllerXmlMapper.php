<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\controller;

use Ada\adapters\XmlAdapter,
    Ada\mapper\AdaDataMapper,
    Qerapp\qbasic\model\controller\ControllerEntity,
    Qerapp\qbasic\model\module\mapper\ModuleXmlMapper;
/**
 * *****************************************************************************
 * Description of ControllerMapper
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class ControllerXmlMapper extends AdaDataMapper
{

   protected
           $_ModuleMapper;


    public function __construct()
    {
        $data_xml = __DATA__.'xml/qbasic/controllers';
        parent::__construct(new XmlAdapter($data_xml,'controller',false));
        $this->_ModuleMapper = new ModuleXmlMapper();
    }
    
    // find by controller name
    public function getByName(string $controller){
        
        return $this->findOne(['controller_name'=> $controller]);
        
    }
    
    // find by controller and module name
    public function getByNameAndModule(string $controller,string $module){
        
        return $this->findOne(['controller_name'=> $controller,'module'=> $module]);
        
    }
    
    /**
     * Get all controllers module
     * @param string $module
     */
    public function getControllersModule(string $module){
        return $this->findAll(['module'=> $module]);
    }
    
    
    /**
     * -------------------------------------------------------------------------
     * Save entity
     * -------------------------------------------------------------------------
     */
    public function save(ControllerEntity $Controller){
        
        $data = parent::getDataObject($Controller);
        
        if (is_null($Controller->id_controller)) {
            $Controller->id_controller = $this->_Adapter->insert($data);
        } else {
           $this->_Adapter->update($data, ['id_controller' => $Controller->id_controller]);
        }
        
    }
    
     /**
     * -------------------------------------------------------------------------
     * Delete entity
     * -------------------------------------------------------------------------
     * @param mixed $id
     * @return type
     */
    public function delete($id)
    {

        // if $id is a object and a UserEntity  
        if ($id instanceof ControllerEntity) {
            $id = $id->id_controller;
        }

        return $this->_Adapter->delete(['id_controller' => $id]);
    }
    
        /**
     * -------------------------------------------------------------------------
     * Create Entity
     * -------------------------------------------------------------------------
     * @param array $row
     * @return Entity
     */
    protected function createEntity(array $row): ControllerEntity
    {
        $Module = $this->_ModuleMapper->findByName($row['module']);
        $ControllerEntity = new ControllerEntity($row,$Module);
        
        return $ControllerEntity;
    }
    

}
