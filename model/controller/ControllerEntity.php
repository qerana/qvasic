<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\controller;

use \Ada\EntityManager,
Qerapp\qbasic\model\module\entity\ModuleEntity;

/**
 * *****************************************************************************
 * Description of ControllerEntity
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class ControllerEntity extends EntityManager
{

    public
            $Module,
            $controller_path;
    protected
            $_id_controller,
            $_controller_name,
            $_module;

    public function __construct(array $data = [], ModuleEntity $Module = null)
    {
        (!empty($data)) ? $this->populate($data) : '';
        (!is_null($Module)) ? $this->setModule($Module) : '';
    }

    public function setModule($Module)
    {
        $this->Module = $Module;
        $this->controller_path = realpath($Module->module_path.'/controller/');
    }

    public function set_id_controller($_id_controller)
    {
        $this->_id_controller = $_id_controller;
    }

    /**
     * -------------------------------------------------------------------------
     * set controller name
     * -------------------------------------------------------------------------
     * @param type $_controller_name_name
     */
    public function set_controller_name($_controller_name_name)
    {
        // only letters in controller name 
        if (ctype_alpha($_controller_name_name) == 1) {
            $this->_controller_name = $_controller_name_name;
        } else {
            throw new \InvalidArgumentException('Only letters in controller name, resolve this, and try again :-)');
        }
    }

    public function set_module($_module)
    {
        $this->_module = $_module;
    }

    public function get_id_controller()
    {
        return $this->_id_controller;
    }

    public function get_controller_name()
    {
        return $this->_controller_name;
    }

    public function get_module()
    {
        return $this->_module;
    }

}
