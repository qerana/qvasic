<?php

/*
 * Copyright (C) 2020 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\log\repository;

use Qerapp\qbasic\model\log\entity\LogInterface;

/*
  |*****************************************************************************
  | LogRepositoryInterface
  |*****************************************************************************
  |
  | Repository INTERFACE Log
  | @author TUPA,
  | @date 2019-08-22 19:44:08,
  |*****************************************************************************
 */

interface LogRepositoryInterface
{

    public function findAll(array $conditions = [], array $options = []);

    public function findById_log(int $id_log, array $options = []);

    public function findById_user(int $id_user, array $options = []);

    public function findByLog_type(string $log_type, array $options = []);

    public function findByMessage_log(string $message_log, array $options = []);

    public function findByLog_timestamp(string $log_timestamp, array $options = []);

    public function findBySw_successfull(string $sw_successfull, array $options = []);

    public function findByUser_agent_log(string $user_agent_log, array $options = []);

    public function findByAddress_ip(string $address_ip, array $options = []);

    public function store(LogInterface $User);

    public function remove($id);
    
    public function removeByConditions(array $conditions);
}
