<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\log\entity;

use \Ada\EntityManager;

/*
  |*****************************************************************************
  | ENTITY CLASS for Log
  |*****************************************************************************
  |
  | Entity Log
  | @author TUPA,
  | @date 2019-08-22 19:44:08,
  |*****************************************************************************
 */

class LogEntity extends EntityManager implements LogInterface,\JsonSerializable
{

    //ATRIBUTES
    protected
/** @var varchar(45), $instance  */ 
$_instance,
    /** @var int(11), $id_log  */
            $_id_log,
            /** @var int(11), $id_user  */
            $_id_user,
            /** @var varchar(100), $log_type  */
            $_log_type,
            /** @var text(), $message_log  */
            $_message_log,
            /** @var timestamp(), $log_timestamp  */
            $_log_timestamp,
            /** @var tinyint(1), $sw_successfull  */
            $_sw_successfull,
            /** @var text(), $user_agent_log  */
            $_user_agent_log,
            /** @var text(), $address_ip  */
            $_address_ip;

    public function __construct(array $data = [])
    {

        $this->populate($data);
    }

//SETTERS

/** 
* ------------------------------------------------------------------------- 
* Setter for instance
* ------------------------------------------------------------------------- 
* @param string 
*/ 
  public function set_instance($instance = ""):void
{ 
$this->_instance = filter_var($instance,FILTER_SANITIZE_STRING);
}
    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_log
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_log($id_log): void
    {
       
        $this->_id_log = $id_log;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_user
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_user($id_user): void
    {
        $this->_id_user = $id_user;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for log_type
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_log_type($log_type): void
    {
        $this->_log_type = $log_type;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for message_log
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_message_log($message_log): void
    {
        $this->_message_log = $message_log;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for log_timestamp
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_log_timestamp($log_timestamp): void
    {
        $this->_log_timestamp = $log_timestamp;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for sw_successfull
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_sw_successfull($sw_successfull): void
    {
        $this->_sw_successfull = $sw_successfull;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for user_agent_log
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_user_agent_log($user_agent_log): void
    {
        $this->_user_agent_log = $user_agent_log;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for address_ip
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_address_ip($address_ip): void
    {
        $this->_address_ip = $address_ip;
    }

//GETTERS

/** 
* ------------------------------------------------------------------------- 
* Getter for instance
* ------------------------------------------------------------------------- 
* @return string  
*/ 
  public function get_instance()
{ 
 return  filter_var($this->_instance,FILTER_SANITIZE_STRING);
}

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_log
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_log()
    {
        return $this->_id_log;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_user
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_user()
    {
        return $this->_id_user;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for log_type
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_log_type()
    {
        return $this->_log_type;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for message_log
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_message_log()
    {
        return $this->_message_log;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for log_timestamp
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_log_timestamp()
    {
        return \helpers\Date::toString($this->_log_timestamp,'d-m-Y H:i:s');
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for sw_successfull
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_sw_successfull()
    {
        return $this->_sw_successfull;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for user_agent_log
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_user_agent_log()
    {
        return $this->_user_agent_log;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for address_ip
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_address_ip()
    {
        return $this->_address_ip;
    }

}
