<?php

/*
 * Copyright (C) 2020 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\log\mapper;

use Ada\adapters\PDOAdapter,
    Ada\mapper\AdaDataMapper,
    Ada\adapters\XmlAdapter,
    Qerapp\qbasic\model\log\entity\LogEntity,
    Qerapp\qbasic\model\log\entity\LogInterface;

/*
  |*****************************************************************************
  | MAPPER CLASS for LogEntity
  |*****************************************************************************
  |
  | MAPPER Log
  | @author TUPA,
  | @date 2019-08-22 19:44:08,
  |*****************************************************************************
 */

class LogMapper extends AdaDataMapper implements LogMapperInterface {

    public function __construct() {
        $this->_Adapter = new PDOAdapter(\Ada\SqlPDO::singleton(), 'qer_log');
        parent::__construct($this->_Adapter);
    }

    /**
     * -------------------------------------------------------------------------
     * Find by id
     * -------------------------------------------------------------------------
     * @param int $id
     */
    public function findById(int $id) {

        $row = $this->_Adapter->find(['id_log' => $id], ['fetch' => 'one']);
        if (!$row) {
            return null;
        } else {
            return $this->createEntity($row);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Save entity
     * -------------------------------------------------------------------------
     */
    public function save(LogInterface $Log) {

        $data = parent::getDataObject($Log);

        if (is_null($Log->id_log)) {
            $Log->id_log = $this->_Adapter->insert($data);
        } else {
            $this->_Adapter->update($data, ['id_log' => $Log->id_log]);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Delete entity
     * -------------------------------------------------------------------------
     * @param mixed $id
     * @return type
     */
    public function delete($id) {

        // if $id is a object and a UserEntity  
        if ($id instanceof LogInterface) {
            $id = $id->id;
        }

        return $this->_Adapter->delete(['id' => $id]);
    }

    /**
     * Delete by conditions
     * @param array $conditions
     */
    public function deleteByConditions(array $conditions) {
        
        return $this->_Adapter->delete($conditions);
        
    }

    /**
     * -------------------------------------------------------------------------
     * Create Entity
     * -------------------------------------------------------------------------
     * @param array $row
     * @return Entity
     */
    protected function createEntity(array $row): LogEntity {

        return new LogEntity($row);
    }

}
