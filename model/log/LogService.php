<?php

/*
 * Copyright (C) 2020 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\log;

use Qerapp\qbasic\model\log\entity\LogEntity AS LogEntity,
    Qerapp\qbasic\model\log\mapper\LogMapper AS LogMapper,
    Qerapp\qbasic\model\log\mapper\LogMapperInterface,
    Qerapp\qbasic\model\log\repository\LogRepository AS LogRepository;

/*
  |*****************************************************************************
  | LogService
  |*****************************************************************************
  |
  | Service for Entity Log
  | @author TUPA,
  | @date 2019-08-22 19:44:08,
  |*****************************************************************************
 */

class LogService {

    public
            $log_type,
            $message_log,
            $instance,
            $sw_successfull,
            $id_user;
    protected
            $Log,
            $_LogRepository;

    public function __construct(LogMapperInterface $Mapper = null) {
        $MapperRepository = (is_null($Mapper)) ? new LogMapper() : $Mapper;
        $this->_LogRepository = new LogRepository($MapperRepository);
    }

    public function getByType(string $type) {
        return $this->_LogRepository->findByLog_type($type);
    }

    /**
     * -------------------------------------------------------------------------
     * Get all
     * -------------------------------------------------------------------------
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getAll($json = false) {

        $Collection = $this->_LogRepository->findAll();
        return ($json === true) ? \helpers\Utils::parseToJson($Collection) : $Collection;
    }

    /**
     * -------------------------------------------------------------------------
     * Get by id
     * -------------------------------------------------------------------------
     * @param $id , id entity
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getById(int $id, $json = false) {

        $Entity = $this->_LogRepository->findById($id);
        return ($json === true) ? \helpers\Utils::parseToJson($Entity) : $Entity;
    }

    /**
     * Get user logs
     * @param int $id_user
     * @return type
     */
    public function getUserLog(int $id_user) {

        return $this->_LogRepository->findById_user($id_user);
    }

    /**
     * -------------------------------------------------------------------------
     * Register new log
     * -------------------------------------------------------------------------
     */
    public function registerLog() {

        $this->createLogEntity();
        $this->_LogRepository->store($this->Log);
    }

    /**
     * get access
     * @return type
     */
    public function getAccess($limit = 10) {

        $conditions = [
            'log_type' => $this->log_type,
            'id_user' => $this->id_user,
        ];

        return $this->_LogRepository->findAll($conditions, ['orderby' => 'log_timestamp DESC ', 'limit' => $limit]);
    }

    /**
     * Create log entity
     */
    private function createLogEntity() {
        $data = [
            'address_ip' => filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_VALIDATE_IP),
            'log_type' => $this->log_type,
            'sw_successfull' => $this->sw_successfull,
            'message_log' => $this->message_log,
            'user_agent_log' => filter_input(INPUT_SERVER, 'HTTP_USER_AGENT', FILTER_SANITIZE_SPECIAL_CHARS),
            'log_timestamp' => date('Y-m-d H:i:s'),
            'id_user' => $this->id_user,
            'instance' => $this->instance
        ];
        try {
            $this->Log = new LogEntity($data);
        } catch (InvalidArgumentException $ex) {
            echo $ex;
        }
    }

    /**
     * register access only if not exists, otherwise, update
     */
    public function registerAccess() {

        $LogRepository = new LogRepository(new LogMapper);
        $conditions = [
            'log_type' => $this->log_type,
            'id_user' => $this->id_user,
            'instance' => $this->instance
        ];
        $Log = $LogRepository->findAll($conditions, ['fetch' => 'one']);

        if (!$Log instanceof entity\LogInterface) {
            $this->registerLog();
        } else {
            // update time access
            $Log->log_timestamp = date('Y-m-d H:i:s');
            $this->_LogRepository->store($Log);
        }
    }

    /**
     * Delete all logs by conditions
     */
    public function deleteAccessInstanceType() {
        
         $LogRepository = new LogRepository(new LogMapper);
        
         $conditions = [
             'instance' => $this->instance,
             'log_type' => $this->log_type
         ];
         
         $LogRepository->removeByConditions($conditions);
        
    }

}
