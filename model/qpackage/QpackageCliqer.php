<?php

/*
 * This file is part of Qbasic
 * Copyright (C) 2019-2020  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\qpackage;

use Qerapp\qbasic\model\module\ModuleService,
    Qerapp\qbasic\model\modeling\ModelService,
    Qerapp\qbasic\model\modeling\EntityService,
    Qerapp\qbasic\model\controller\ControllerService,
    \Ada\adapters\XmlAdapter;

/**
 * *****************************************************************************
 * Description of Qinstaller
 * *****************************************************************************
 * Installs packages for qerana 
 * 
 * @author diemarc
 * *****************************************************************************
 */
class QpackageCliqer {

    private
            $ModuleService;
    protected
            $pkg_tmp_path,
            $pkg_name,
            $pkg_params,
            $pkg_type,
            $pkg_models = [],
            $pkg_entities = [],
            $pkg_controllers = [];

    public function __construct($options) {

        $this->pkg_name = $options['name'];

        if (!empty($this->pkg_name)) {

            echo '__________________________________________________________________' . "\n";
            echo '          ' . $this->pkg_name . ' INSTALLER' . "\n";
            echo '__________________________________________________________________' . "\n";
            echo "\n";


            $this->ModuleService = new ModuleService();
            $this->ModuleService->module_name = $this->pkg_name;
            $Module = $this->ModuleService->getModule();

            if ($Module) {

                echo '----------------PACKAGE ALREADY INSTALLED-----------------' . "\n\n";
                print_r($Module);
                die();
            }


            if ($this->pkg_name == 'qbasic') {
                echo '----------------Qbasic INSTALLATION SUCCESFUL-----------------' . "\n";
                exit();
            }

            $this->pkg_tmp_path = realpath(__QERAPPSFOLDER__ . '' . $this->pkg_name);

            if (empty($this->pkg_tmp_path)) {
                echo '----------------Package files missing!-----------------' . "\n";
                exit();
            }


            $this->pkg_params = parse_ini_file($this->pkg_tmp_path . '/.install/config.ini', true);
            $this->pkg_type = $this->pkg_params['basic']['type'];



            // print_r($this->pkg_params);
        } else {
            die("Installer fail!!, no package name was defined!!!");
        }
    }

    /**
     * -------------------------------------------------------------------------
     * install a package
     * -------------------------------------------------------------------------
     */
    public function install() {


        // register module
        $ModuleService = new ModuleService();
        $ModuleService->module_name = ($this->pkg_type == 'qerapp') ? substr($this->pkg_name, 1) : $this->pkg_name;
        $ModuleService->module_type = $this->pkg_type;
        $ModuleService->createModuleEntity();

        $this->setComponents();

        // move package
       // $this->movePackage();


        $this->runSetup();
    }

    /**
     *  Set component if exists in package
     */
    private function setComponents() {

        echo '....Searching components....' . "\n";
        $components_xml = realpath($this->pkg_tmp_path . '/.install/components.xml');
        if (!empty($components_xml)) {
            echo '      ...Components Finded!!....' . "\n";
            // register model
            $this->registerModels();
            // entities
            $this->registerEntities();

            // controllers
            $this->registerControllers();
        } else {
            echo '      ...!Components not found....' . "\n";
        }
    }

    /**
     *  Register Models
     */
    private function registerModels() {

        echo '              ....Searching for models....' . "\n";
        $XmlModel = new XmlAdapter($this->pkg_tmp_path . '/.install/components', 'model', false);
        // get all models
        $rsModels = $XmlModel->find();
        if (!empty($rsModels)) {

            echo '                  [Models components Found!!]' . "\n";

            $nm = 0;
            foreach ($rsModels AS $model):
                $nm++;
                $ModelService = new ModelService();
                $ModelService->setModule($this->pkg_name);
                $ModelService->setModel($model['model_name']);
                echo '                          ' . $nm . '-....Creating model:' . $model['model_name'] . '...' . "\n";
                $ModelService->saveModel();
            endforeach;
        } else {
            echo '                  ...!Models not found....' . "\n";
        }
    }

    /**
     *  Register Entities
     */
    private function registerEntities() {

        echo '              ....Searching for entities....' . "\n";
        $XmlEntity = new XmlAdapter($this->pkg_tmp_path . '/.install/components', 'entity', false);
        // get all models
        $rsEntities = $XmlEntity->find();
        if (!empty($rsEntities)) {

            echo '                  [Entities components Found!!]' . "\n";

            $nm = 0;
            foreach ($rsEntities AS $entity):
                $nm++;
                $EntityService = new EntityService();
                $EntityService->set_entity_name($entity['entity_name']);
                $EntityService->setModel($entity['model']);
                $EntityService->setSource($entity['source']);
                $EntityService->setOrigin($entity['origin']);
                echo '                          ' . $nm . '-....Creating Entity:' . $entity['entity_name'] . '...' . "\n";
                $EntityService->saveEntity();
            endforeach;
        } else {
            echo '                  ...!Entities not found....' . "\n";
        }
    }

    /**
     *  Register Controllers
     */
    private function registerControllers() {

        echo '              ....Searching for controllers....' . "\n";
        $XmlController = new XmlAdapter($this->pkg_tmp_path . '/.install/components', 'controller', false);
        // get all models
        $rsControllers = $XmlController->find();
        if (!empty($rsControllers)) {

            echo '                  [Controllers components Found!!]' . "\n";

            $nm = 0;
            foreach ($rsControllers AS $controller):
                $nm++;
                $ControllerService = new ControllerService;
                $ControllerService->setModule($controller['module']);
                $ControllerService->set_controller_name($controller['controller_name']);
                echo '                          ' . $nm . '-....Creating Controller:' . $controller['controller_name'] . '...' . "\n";
                $ControllerService->saveController();
            endforeach;
        } else {
            echo '                  ...!Controllers not found....' . "\n";
        }
    }

    /**
     *  run aditional package setup
     */
    private function runSetup() {

        $base_package = ($this->pkg_type == 'qerapp') ? __QERAPPSFOLDER__ : __APPFOLDER__;
        
        
        require $base_package. $this->pkg_name.'/.install/Setup.php';
        $Setup = new \Setup;
        $Setup->run();
    }

    public function remove() {
        // delete a module 
        $ModuleService = new ModuleService();
        $ModuleService->module_name = ($this->pkg_type == 'qerapp') ? substr($this->pkg_name, 1) : $this->pkg_name;
        $ModuleService->module_type = $this->pkg_type;
        $ModuleService->remove();
    }

    /**
     *  Move package to a destination folder (app or qerapp)
     */
    private function movePackage() {

        if ($this->pkg_type == 'app') {

            $destination_folder = realpath(__APPFOLDER__);
        } else if ($this->pkg_type == 'qerapp') {
            $destination_folder = realpath(__QERAPPSFOLDER__);
        }

        if (realpath($destination_folder . '/' . $this->pkg_name)) {
            echo '----------------Pacakge already exists in destination path-----------------' . "\n";
        } else {
            shell_exec("mv $this->pkg_tmp_path $destination_folder");
        }

        if (realpath($destination_folder . '/' . $this->pkg_name)) {
            echo "\n";
            echo '----------------PACKAGE:' . $this->pkg_name . ' successfully installed -----------------' . "\n";
        }
    }

}
