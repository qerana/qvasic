<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\view;

use Qerapp\qbasic\model\view\ViewService;

/**
 * *****************************************************************************
 * Description of AddFormService
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class FormService
{

    protected
            $_ViewService,
            $_form_elements,
            $_form,
            $_form_template,
            $_form_filename,
            $_form_action,
            $_form_object,
            $_form_key,
            $_form_edit_key,
            $_form_calback_save,
            // index location to create
            $_path_to_new_form,
            $_path_to_edit_form;

    public function __construct(ViewService $ViewService, $form)
    {
        $this->_ViewService = $ViewService;
        $this->_form = $form;
        $this->_form_object = $this->_ViewService->Entity->entity_name;
        $this->_path_to_new_form = $this->_ViewService->view_path . '/add_' . strtolower($this->_ViewService->Entity->entity_name) . '.php';
        $this->_path_to_edit_form = $this->_ViewService->view_path . '/edit_' . strtolower($this->_ViewService->Entity->entity_name) . '.php';




        $this->_setupForm();
    }

    /**
     * -------------------------------------------------------------------------
     * Set the form template to use
     * -------------------------------------------------------------------------
     */
    private function _setupForm()
    {


        $this->_parseAtributesToFormElements();

        switch ($this->_form):

            case 'add';

                // check if exists form add
                if (empty(realpath($this->_path_to_new_form))) {
                    \helpers\File::createFile($this->_path_to_new_form);
                }

                $this->_form_template = __QERAPPSFOLDER__ . 'qbasic/_layouts/view/tpl_form_modal.qer';
                $this->_form_filename = $this->_path_to_new_form;
                $this->_form_action = 'save';
                $this->_form_edit_key = '';
                $this->_form_calback_save = 'load' . $this->_ViewService->Entity->entity_name;
                break;

            case 'edit';
                
                  // check if exists form edit
                if (empty(realpath($this->_path_to_edit_form))) {
                    \helpers\File::createFile($this->_path_to_edit_form);
                }
                
                $this->_form_template = __QERAPPSFOLDER__ . 'qbasic/_layouts/view/tpl_form_modal.qer';
                $this->_form_filename = $this->_path_to_edit_form;
                $this->_form_object = '$' . $this->_ViewService->Entity->entity_name;
                $this->_form_action = 'save';
                $this->_form_edit_key = '<input type="hidden" name="f_' . $this->_ViewService->Entity->entity_key . '" id="f_' . $this->_ViewService->Entity->entity_key . '" value="<?php echo $' . $this->_ViewService->Entity->entity_name. '->' . $this->_ViewService->Entity->entity_key. ';?>">';
                $this->_form_calback_save = 'loadData' . $this->_ViewService->Entity->entity_name;
                break;

        endswitch;
    }

    /**
     * -------------------------------------------------------------------------
     * Parse attributes to form inputs
     * -------------------------------------------------------------------------
     * @param $mode , add = add form, edit= edit form
     */
    private function _parseAtributesToFormElements()
    {

        $AttributeCollection = $this->_ViewService->Entity->entity_attributes;

        foreach ($AttributeCollection AS $Attribute):
            $this->_createFormElement($Attribute);
        endforeach;
    }

    /**
     * -------------------------------------------------------------------------
     * Parse form element for attribute object
     * -------------------------------------------------------------------------
     * @param object $Attribute
     */
    private function _createFormElement(object $Attribute)
    {

        if ($Attribute->obs !== 'auto_increment') {
            $element_to_use = $this->parseAttributeToInput($Attribute);

            $this->_form_elements .= " <div class='form-group form-group-sm row small'> \n";
            $this->_form_elements .= " <label for='f_$Attribute->name' class='col-sm-3 col-form-label'>" . ucwords(str_replace('_', ' ', $Attribute->name)) . "</label>  \n";
            $this->_form_elements .= "  <div class='col-sm-9'>  \n";
            $this->_form_elements .= "  <div class='input-group col-sm-8'>   \n";
            $this->_form_elements .= $element_to_use . "\n";
            $this->_form_elements .= "  </div>   \n";
            $this->_form_elements .= "  </div>   \n";
            $this->_form_elements .= "  </div>   \n";
        } else {
            $this->_form_key = $Attribute->name;
        }
    }

    /**
     * -------------------------------------------------------------------------
     * parse inputs
     * -------------------------------------------------------------------------
     * @param object $Attribute
     * @return type
     */
    private function parseAttributeToInput(object $Attribute)
    {

        $element_name = 'f_' . $Attribute->name;
        $length = $Attribute->length;

        // special type elements
        if (strpos($Attribute->name, 'pass') !== false) {
            $type = 'password';
        } else if (strpos($Attribute->name, 'mail') !== false) {
            $type = 'email';
        } else if (strpos($Attribute->name, 'telefono') !== false) {
            $type = 'tel';
        } else {
            $type = '';
        }

        // required
        $required = ($Attribute->null === 'NO') ? 'required' : '';

        $edit_value = ($this->_form === 'edit') ? ' value=' . "'" . '<?php echo $' . $this->_form_object . '->' . $Attribute->name . ';?>' . "'" : "";
        $textarea_value = ($this->_form === 'edit') ? '<?php echo $' . $this->_form_object . '->' . $Attribute->name . ';?>' : "";

        switch ($Attribute->type) {

            case 'varchar';

                $varchar_type = (empty($type)) ? 'text' : $type;
                $element = "   <input type='$varchar_type' id='$element_name' name='$element_name' 
                                                    
                                                   class='form-control form-control-sm' $required  $edit_value />";

                break;

            case 'int';
                $int_type = (empty($type)) ? 'number' : $type;
                $element = "   <input type='$int_type' id='$element_name' name='$element_name' 
                                                   class='form-control form-control-sm'  $required $edit_value  />";

                break;
            case 'decimal';
                $int_type = (empty($type)) ? 'number' : $type;
                $element = "   <input type='text'  id='$element_name' name='$element_name' 
                                                   class='form-control form-control-sm'  $required $edit_value  />";

                break;
            case 'tinyint';

                $element = "   <input type='number' id='$element_name' name='$element_name' 
                                                   class='form-control form-control-sm'  $required $edit_value  />";

                break;

            case 'date';
                $element = "   <input type='date' id='$element_name' name='$element_name' 
                                                   class='form-control form-control-sm'  $required  $edit_value  />";
                break;

            case 'datetime';

                $element = "   <input type='date' id='$element_name' name='$element_name' 
                                                   class='form-control form-control-sm'  $required $edit_value  />";
                break;

            case 'text';

                $element = "   <textarea id='$element_name' name='$element_name' $required
                                                   class='form-contro form-control-sml' cols='60' rows='5'>$textarea_value</textarea>";
                break;

             case 'longtext';

                $element = "   <textarea id='$element_name' name='$element_name' $required
                                                   class='form-contro form-control-sml' cols='60' rows='5'>$textarea_value</textarea>";
                break;


        }


        return $element;
    }

    /**
     * -------------------------------------------------------------------------
     * Create Form
     * -------------------------------------------------------------------------
     */
    public function create()
    {

        $replaces = [
            '[{title}]' => 'New ' . ucwords($this->_ViewService->Entity->entity_name),
            '[{module}]' => $this->_ViewService->Entity->Model->Module->name,
            '[{controller}]' => $this->_ViewService->Entity->entity_name,
            '[{action}]' => $this->_form_action,
            '[{id_key}]' => $this->_ViewService->Entity->entity_key,
            '[{form_edit_key}]' => $this->_form_edit_key,
            '[{function_name}]' => ucwords($this->_ViewService->Entity->entity_name),
            '[{url_goback}]' => '/' . $this->_ViewService->Entity->Model->Module->name . '/' . strtolower($this->_ViewService->Entity->entity_name) . '/index',
            '[{form}]' => $this->_form_elements,
            '[{callback}]' => $this->_form_calback_save
        ];

        $content = strtr(file_get_contents(realpath($this->_form_template)), $replaces);
        file_put_contents(realpath($this->_form_filename), $content);
    }

}
