<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace Qerapp\qbasic\model\view;

use Qerapp\qbasic\model\view\ViewService;

/**
 * *****************************************************************************
 * Detail service, to create detail view
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class DetailService
{

    const
            _JSON_LAYOUT_ = __QERAPPSFOLDER__ . 'qbasic/_layouts/view/tpl_detail_json.qer',
            _DEFAULT_LAYOUT_ = __QERAPPSFOLDER__ . 'qbasic/_layouts/view/tpl_detail_default.qer';

    protected
            $_ViewService,
            $_path_to_detail,
            // default values, or values vy json (default,json)
            $_type,
            // layout to use, json or default layout
            $_layout,
            // attributes parsed in html
            $_fields_html;

    public function __construct(ViewService $ViewService, string $type)
    {
        $this->_ViewService = $ViewService;
        $this->_setType($type);
        $this->_path_to_detail = $this->_ViewService->view_path . '/detail_' . strtolower($this->_ViewService->Entity->entity_name) . '.php';
    }

    /**
     * Set the type and layout to use
     * @param string $type
     */
    protected function _setType(string $type)
    {
        $this->_type = $type;

        if ($this->_type === 'JSON') {
            $this->_layout = self::_JSON_LAYOUT_;
        } else if ($this->_type === 'DEFAULT') {
            $this->_layout = self::_DEFAULT_LAYOUT_;
            // parse each attr
            $this->_parseAttributesToHtml();
        }
    }

    /**
     * Parse each attribute to html field
     */
    protected function _parseAttributesToHtml()
    {

        $Attributes = $this->_ViewService->Entity->entity_attributes;

        foreach ($Attributes AS $Attribute):

            $this->_createFieldHtml($Attribute);

        endforeach;
    }

    /**
     * Parse an attribute to html field
     * @param object $Attribute
     */
    private function _createFieldHtml(object $Attribute)
    {

        $this->_fields_html .= ' <div class="row m-1">'."\n"
                . ' <div class="col-sm-3 bg-gray-100 p-2">'."\n"
                . '         <b>'.str_replace('_', ' ', $Attribute->name).'</b>'."\n"
                . ' </div>'."\n"
                . '    <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_' .$Attribute->name. '">'."\n"
                . '        <?php echo $'.$this->_ViewService->Entity->entity_name.'->'.$Attribute->name.';?>'."\n"
                . '    </div>'."\n"
                . '</div>'."\n";
    }

    /**
     * -------------------------------------------------------------------------
     * Create Form
     * -------------------------------------------------------------------------
     */
    public function create()
    {

        if (!is_file($this->_path_to_detail)) {
            \helpers\File::createFile($this->_path_to_detail);
        }

        $replaces = [
            '[{title}]' => 'New ' . $this->_ViewService->Entity->entity_name,
            '[{module}]' => $this->_ViewService->Entity->Model->Module->name,
            '[{entity}]' => $this->_ViewService->Entity->entity_name,
            '[{key}]' => $this->_ViewService->Entity->entity_key,
            '[{fields}]' => $this->_fields_html
        ];

        $content = strtr(file_get_contents(realpath($this->_layout)), $replaces);
        file_put_contents(realpath($this->_path_to_detail), $content);
    }

}
