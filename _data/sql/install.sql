CREATE TABLE `qer_log` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `log_type` varchar(45) NOT NULL,
  `message_log` text,
  `log_timestamp` datetime NOT NULL,
  `sw_successfull` varchar(45) DEFAULT NULL,
  `user_agent_log` text,
  `address_ip` varchar(45) DEFAULT NULL,
  `instance` varchar(45) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_log`),
  KEY `idx_id_user` (`id_user`) USING BTREE,
  KEY `idx_log_type` (`log_type`),
  KEY `idx_instance` (`instance`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
