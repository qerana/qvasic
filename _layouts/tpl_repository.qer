<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace [{repository_namespace}];
use [{mapper_interface_namespace}],
[{repository_namespace_interface}]\[{repository_name}]Interface,
[{entity_interface_namespace}];
/*
  |*****************************************************************************
  | [{repository_name}]Repository
  |*****************************************************************************
  |
  | Repository [{repository_name}]
  | @author qDevTools,
  | @date [{date}],
  |*****************************************************************************
 */

class [{repository_name}] implements [{repository_name}]Interface
{
    
    private
            $_[{Entity}]Mapper;
            
    public function __construct([{mapper}] $Mapper)
    {
        
         $this->_[{Entity}]Mapper = $Mapper;
        
    }
          /**
     * -------------------------------------------------------------------------
     * Get One
     * -------------------------------------------------------------------------
     * @return [{repository_name}]
     */
    
     public function find(array $condition = [],array $options = []) {
        return $this->_[{Entity}]Mapper->findOne($condition,$options);
    }
    
    
      /**
     * -------------------------------------------------------------------------
     * Get all [{repository_name}]
     * -------------------------------------------------------------------------
     * @return [{repository_name}]Entity collection
     */
    
    public function findById(int $id)
    {
        return $this->_[{Entity}]Mapper->findOne(['[{Entity_key}]'=>$id]);
    }
    
     /**
     * -------------------------------------------------------------------------
     * Get all [{repository_name}]
     * -------------------------------------------------------------------------
     * @return [{repository_name}]Entity collection
     */
    
    public function findAll(array $conditions = [],array $options = [])
    {
        return $this->_[{Entity}]Mapper->findAll($conditions,$options);
    }
    
    
    [{finders}]
    
   
    
     /**
     * -------------------------------------------------------------------------
     * Save [{repository_name}]
     * -------------------------------------------------------------------------
     * @object $[{repository_name}]Entity
     * @return type
     */
    public function store([{Entity}]Interface $[{repository_name}]Entity)
    {
        return $this->_[{Entity}]Mapper->save($[{repository_name}]Entity);
    }
    
     /**
     * -------------------------------------------------------------------------
     * Delete [{repository_name}]
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function remove($id)
    {
        return $this->_[{Entity}]Mapper->delete($id);
    }
 
}
