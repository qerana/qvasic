<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace [{namespace}];

use [{service_namespace}];


defined('__APPFOLDER__') OR exit('Direct access to this file is forbidden, siya');
/*
  |*****************************************************************************
  | CONTROLLER CLASS  [{controller_name}]
  |*****************************************************************************
  |
  | Controller [{controller_name}]
  | @author qDevTools,
  | @date [{date}],
  |*****************************************************************************
 */

class [{controller_name}]Controller extends \Qerana\core\QeranaC {

    protected
            $_[{service_name}];

     public function __construct()
    {
        parent::__construct();
        $this->_[{service_name}] = new [{service_name}];
    }
    
    
     /**
     * -------------------------------------------------------------------------
     * Get all in json
     * -------------------------------------------------------------------------
     */
    public function getAllInJson(){
        return $this->_[{service_name}]->getAll(true);
    }
    
    /**
     * -------------------------------------------------------------------------
     * Get one in json
     * -------------------------------------------------------------------------
     */
    public function getOneInJson(int $id){
        return $this->_[{service_name}]->getById($id,true);
    }
    
    
    /**
     * -------------------------------------------------------------------------
     * Show all
     * @return void
     * -------------------------------------------------------------------------
     */
    public function index():void{
      
        
        $vars = [
            'Plugins' => [
                 'data_json.js',
                'app/[{controller_folder}].js'
            ]
        ];
      \Qerana\core\View::showView('[{controller_folder}]/index_[{controller_folder}]', $vars);
    }
    
    
   
    
    /**
     * -------------------------------------------------------------------------
     * Add new 
     * @return void
     * -------------------------------------------------------------------------
     */
    
    public function add() : void{

        $vars = [];
        
         \Qerana\core\View::showForm('[{controller_folder}]/add_[{controller_folder}]',$vars);
    }
    
     /**
     * -------------------------------------------------------------------------
     * Save new 
     * @return void
     * -------------------------------------------------------------------------
     */
    
    public function save():void{
        
        $this->_[{service_name}]->save();
  
    }
    
    /**
     * -------------------------------------------------------------------------
     * View
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    
    public function detail(int $id) : void{
        
        
       $vars = [
            '[{entity_key}]' => $id,
            '[{entity_name}]' => $this->_[{service_name}]->getById($id),
            'Plugins' => [
                 'data_json.js',
                'app/[{controller_folder}].js'
            ]
        ];
        
          \Qerana\core\View::showView('[{controller_folder}]/detail_[{controller_folder}]',$vars);
        
    }
    
    /**
     * -------------------------------------------------------------------------
     * Edit 
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    
    public function edit(int $id) :void{
        
        $vars = [
            '[{entity_name}]' => $this->_[{service_name}]->getById($id),
            'Plugins' => [
                 'data_json.js',
                'app/[{controller_folder}].js'
            ]
        ];
        \Qerana\core\View::showForm('[{controller_folder}]/edit_[{controller_folder}]',$vars);
    }
    
    /**
     * -------------------------------------------------------------------------
     * modify
     * -------------------------------------------------------------------------
     * @return void
     */
    
    public function modify():void{
          $this->_[{service_name}]->save();
    }
    
    /**
     * -------------------------------------------------------------------------
     * Delete
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    
    public function delete(int $id):void{
          $this->_[{service_name}]->delete($id);
    }
    
   

}